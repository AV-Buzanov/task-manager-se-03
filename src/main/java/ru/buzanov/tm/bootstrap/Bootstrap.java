package ru.buzanov.tm.bootstrap;

import ru.buzanov.tm.constant.CommandConst;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;

public class Bootstrap {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private TaskService taskService = new TaskService(projectRepository, taskRepository);

    public void start() {
        boolean out = true;
        String buffer = "";

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (out) {

            try {
                buffer = reader.readLine();

                switch (buffer) {
                    case (CommandConst.CREATE_PROJECT):
                        projectService.create();
                        break;

                    case (CommandConst.CLEAR_PROJECT):
                        projectService.clear();
                        break;

                    case (CommandConst.LIST_PROJECT):
                        projectService.list();
                        break;

                    case (CommandConst.RENAME_PROJECT):
                        projectService.rename();
                        break;

                    case (CommandConst.REMOVE_PROJECT):
                        projectService.remove();
                        break;

                    case (CommandConst.VIEW_PROJECT):
                        projectService.view();
                        break;

                    case (CommandConst.CREATE_TASK):
                        taskService.create();
                        break;

                    case (CommandConst.CLEAR_TASK):
                        taskService.clear();
                        break;

                    case (CommandConst.LIST_TASK):
                        taskService.list();
                        break;

                    case (CommandConst.RENAME_TASK):
                        taskService.rename();
                        break;

                    case (CommandConst.VIEW_TASK):
                        taskService.view();
                        break;

                    case (CommandConst.REMOVE_TASK):
                        taskService.remove();
                        break;

                    case (CommandConst.ATTACH_TASK):
                        taskService.attachProject();
                        break;

                    case (CommandConst.DISCONNECT_TASK):
                        taskService.disconnectProject();
                        break;

                    case (CommandConst.HELP):
                        helpOut();
                        break;

                    case (CommandConst.EXIT):
                        out = false;
                        break;
                }
            } catch (ParseException e) {
                System.out.println("Wrong input, try again");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selected index doesn't exist");
            } catch (Exception e) {
                System.out.println("Something wrong, try again");
            }
        }
    }

    private static void helpOut() {
        System.out.println(CommandConst.HELP + ": Show all commands.");
        System.out.println(CommandConst.CLEAR_PROJECT + ": Remove all projects.");
        System.out.println(CommandConst.CREATE_PROJECT + ": Create new project.");
        System.out.println(CommandConst.LIST_PROJECT + ": Show all projects.");
        System.out.println(CommandConst.REMOVE_PROJECT + ": Remove selected project with connected tasks.");
        System.out.println(CommandConst.RENAME_PROJECT + ": Rename project");
        System.out.println(CommandConst.VIEW_PROJECT + ": View project information");
        System.out.println(CommandConst.CLEAR_TASK + ": Remove all tasks.");
        System.out.println(CommandConst.CREATE_TASK + ": Create new task.");
        System.out.println(CommandConst.LIST_TASK + ": Show all tasks.");
        System.out.println(CommandConst.REMOVE_TASK + ": Remove selected tasks.");
        System.out.println(CommandConst.RENAME_TASK + ": Rename task");
        System.out.println(CommandConst.ATTACH_TASK + ": Attach task to project");
        System.out.println(CommandConst.DISCONNECT_TASK + ": Disconnect project from task");
        System.out.println(CommandConst.VIEW_TASK + ": View task information");
        System.out.println(CommandConst.EXIT + ": Close program");
    }
}