package ru.buzanov.tm.service;

import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ProjectService {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private int indexBuf;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public ProjectService() {
    }

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String buffer = reader.readLine();

        for (int i = 0; i < projectRepository.getSize(); i++) {
            if (buffer.equals(projectRepository.getByCount(i).getName())) {
                System.out.println("Project with this name already exist.");
                return;
            }
        }

        indexBuf = projectRepository.create();
        projectRepository.getByCount(indexBuf).setName(buffer);
        System.out.println("ENTER PROJECT START DATE (DD MM YYYY):");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            projectRepository.getByCount(indexBuf).setStartDate(dateFormat.parse(buffer));
        System.out.println("ENTER PROJECT END DATE (DD MM YYYY):");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            projectRepository.getByCount(indexBuf).setEndDate(dateFormat.parse(buffer));
        System.out.println("ENTER DESCRIPTION:");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            projectRepository.getByCount(indexBuf).setDescription(buffer);
        System.out.println("[OK]");
    }

    public void clear() {
        projectRepository.deleteAll();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void list() {
        System.out.println("[PROJECT LIST]");
        outList();
    }

    public void rename() throws Exception {
        System.out.println("[RENAME PROJECT]");
        System.out.println("Choose index number of project to rename");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        System.out.println("Write new name");
        projectRepository.getByCount(indexBuf).setName(reader.readLine());
        System.out.println("[PROJECT RENAMED]");
    }

    public void remove() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Choose index number of project to remove");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        String projectId = projectRepository.getByCount(indexBuf).getId();
        taskRepository.deleteByProjectId(projectId);
        projectRepository.deleteByCount(indexBuf);
        System.out.println("[PROJECT REMOVED WITH CONNECTED TASKS]");
    }

    public void view() throws Exception {
        System.out.println("[VIEW PROJECT]");
        System.out.println("Choose index number of project");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        System.out.println("PROJECT NAME:");
        System.out.println(projectRepository.getByCount(indexBuf).getName());
        System.out.println("PROJECT START DATE:");
        if (projectRepository.getByCount(indexBuf).getStartDate() != null)
            System.out.println(dateFormat.format(projectRepository.getByCount(indexBuf).getStartDate()));
        System.out.println("PROJECT END DATE:");
        if (projectRepository.getByCount(indexBuf).getEndDate() != null)
            System.out.println(dateFormat.format(projectRepository.getByCount(indexBuf).getEndDate()));
        System.out.println("PROJECT DESCRIPTION:");
        if (projectRepository.getByCount(indexBuf).getDescription() != null)
            System.out.println(projectRepository.getByCount(indexBuf).getDescription());
        System.out.println("PROJECT TASKS:");
        for (Task task : taskRepository.getTasksByProjectId(projectRepository.getByCount(indexBuf).getId())) {
            System.out.println(task.getName());
        }
    }

    private void outList() {
        for (int i = 0; i < projectRepository.getSize(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectRepository.getByCount(i).getName());
        }
    }
}