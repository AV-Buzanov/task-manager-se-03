package ru.buzanov.tm.service;

import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class TaskService {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private int indexBuf;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    private ProjectRepository projectDAO;

    private TaskRepository taskDAO;

    public TaskService() {
    }

    public TaskService(ProjectRepository projectDAO, TaskRepository taskDAO) {
        this.projectDAO = projectDAO;
        this.taskDAO = taskDAO;
    }

    public void create() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String buffer = reader.readLine();

        for (int i = 0; i < taskDAO.getSize(); i++) {
            if (buffer.equals(taskDAO.getByCount(i).getName())) {
                System.out.println("Task with this name already exist.");
                return;
            }
        }
        indexBuf = taskDAO.create();
        taskDAO.getByCount(indexBuf).setName(buffer);
        System.out.println("ENTER TASK START DATE (DD MM YYYY):");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            taskDAO.getByCount(indexBuf).setStartDate(dateFormat.parse(buffer));
        System.out.println("ENTER TASK END DATE (DD MM YYYY):");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            taskDAO.getByCount(indexBuf).setEndDate(dateFormat.parse(buffer));
        System.out.println("ENTER DESCRIPTION:");
        buffer = reader.readLine();
        if (!"".equals(buffer))
            taskDAO.getByCount(indexBuf).setDescription(buffer);

        System.out.println("CHOOSE PROJECT:");
        for (int i = 0; i < projectDAO.getSize(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectDAO.getByCount(i).getName());
        }
        buffer = reader.readLine();
        String projectId;
        if (!"".equals(buffer)) {
            projectId = projectDAO.getByCount(Integer.parseInt(buffer)).getId();
            taskDAO.getByCount(indexBuf).setProjectId(projectId);
        }
        System.out.println("[OK]");
    }

    public void clear() {
        taskDAO.deleteAll();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void list() {
        System.out.println("[TASK LIST]");
        outList();
    }

    public void rename() throws Exception {
        System.out.println("[RENAME TASK]");
        System.out.println("Choose index number of task to rename");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        System.out.println("Write new name");
        taskDAO.getByCount(indexBuf).setName(reader.readLine());
        System.out.println("[TASK RENAMED]");
    }

    public void remove() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("Choose index number of task to remove");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        taskDAO.deleteByCount(Integer.parseInt(reader.readLine()));
        System.out.println("[TASK REMOVED]");
    }

    public void view() throws Exception {
        System.out.println("[VIEW TASK]");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        System.out.println("TASK NAME:");
        System.out.println(taskDAO.getByCount(indexBuf).getName());
        System.out.println("TASK START DATE:");
        if (taskDAO.getByCount(indexBuf).getStartDate() != null)
            System.out.println(dateFormat.format(taskDAO.getByCount(indexBuf).getStartDate()));
        System.out.println("TASK END DATE:");
        if (taskDAO.getByCount(indexBuf).getEndDate() != null)
            System.out.println(dateFormat.format(taskDAO.getByCount(indexBuf).getEndDate()));
        System.out.println("TASK DESCRIPTION:");
        if (taskDAO.getByCount(indexBuf).getDescription() != null)
            System.out.println(taskDAO.getByCount(indexBuf).getDescription());
        System.out.println("TASK PROJECT:");
        if (taskDAO.getByCount(indexBuf).getProjectId() != null) {
            String projectId = taskDAO.getByCount(indexBuf).getProjectId();
            System.out.println(projectDAO.getById(projectId).getName());
        }
    }

    public void attachProject() throws Exception {
        System.out.println("[ATTACH TASK TO PROJECT]");
        System.out.println("CHOOSE TASK");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        System.out.println("CHOOSE PROJECT");
        for (int i = 0; i < projectDAO.getSize(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectDAO.getByCount(i).getName());
        }
        String projectId = projectDAO.getByCount(Integer.parseInt(reader.readLine())).getId();
        taskDAO.getByCount(indexBuf).setProjectId(projectId);
        System.out.println("[OK]");
    }

    public void disconnectProject() throws Exception {
        System.out.println("[DISCONNECT PROJECT FROM TASK]");
        System.out.println("CHOOSE TASK");
        outList();
        indexBuf = Integer.parseInt(reader.readLine());
        taskDAO.getByCount(indexBuf).setProjectId(null);
        System.out.println("[OK]");
    }

    private void outList() {
        for (int i = 0; i < taskDAO.getSize(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskDAO.getByCount(i).getName());
        }
    }
}