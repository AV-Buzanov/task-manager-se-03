package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    private List<Project> projectList = new ArrayList<>();

    public int create() {
        projectList.add(new Project());
        return projectList.size() - 1;
    }

    public boolean deleteById(String id) {
        for (int i = 0; i < projectList.size(); i++) {
            if (projectList.get(i).getId().equals(id)) {
                projectList.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean deleteByCount(int count) {
        try {
            projectList.remove(count);
            return true;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    public Project getById(String id) {
        for (int i = 0; i < projectList.size(); i++) {
            if (projectList.get(i).getId().equals(id)) {
                return projectList.get(i);
            }
        }
        return null;
    }

    public Project getByCount(int count) {
        try {
            return projectList.get(count);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public void deleteAll() {
        projectList.clear();
    }

    public int getSize() {
        return projectList.size();
    }
}