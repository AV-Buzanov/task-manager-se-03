package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskRepository {

    private List<Task> taskList = new ArrayList<>();

    public int create() {
        taskList.add(new Task());
        return taskList.size() - 1;
    }

    public boolean deleteById(String id) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getId().equals(id)) {
                taskList.remove(i);
                return true;
            }
        }
        return false;
    }

    public void deleteByProjectId(String projectId) {
        Iterator<Task> taskIterator = taskList.iterator();
        while (taskIterator.hasNext()) {
            Task nextTask = taskIterator.next();
            if (projectId.equals(nextTask.getProjectId()))
                taskIterator.remove();
        }
    }

    public boolean deleteByCount(int count) {
        try {
            taskList.remove(count);
            return true;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    public Task getById(String id) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getId().equals(id)) {
                return taskList.get(i);
            }
        }
        return null;
    }

    public Task getByCount(int count) {
        try {
            return taskList.get(count);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public void deleteAll() {
        taskList.clear();
    }

    public int getSize() {
        return taskList.size();
    }

    public List<Task> getTasksByProjectId(String id) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : this.taskList) {
            if (id.equals(task.getProjectId()))
                taskList.add(task);
        }
        return taskList;
    }
}
